package com.zuit.wdc044.exceptions;

public class UserException extends Exception{
    public UserException(String message){
        //Super method - it will inherit parent's class properties and methods.
        super(message);
    }

}
