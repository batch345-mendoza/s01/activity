package com.zuit.wdc044.services;

import com.zuit.wdc044.models.User;
import java.util.Optional;
public interface UserService {
    //We just created a default service;
    void createUser(User user);


    //We created a default service where in it will return the record that will match the username provided in the parameter. otherwise, optional it will return null.
    Optional<User> findByUsername(String username);
}
