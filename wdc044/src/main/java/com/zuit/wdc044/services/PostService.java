package com.zuit.wdc044.services;

import com.zuit.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    void createPost(String stringToken, Post post);

    //Iterable - iterate collection of the posts
    Iterable<Post> getPosts();

    ResponseEntity<?> updatePost(Long id, String stringToken, Post post);

    ResponseEntity<?> deletePost(Long id, String stringToken);

    Iterable<Post> getMyPosts(String stringToken);


}
