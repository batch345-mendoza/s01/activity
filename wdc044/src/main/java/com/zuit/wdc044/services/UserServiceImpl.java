package com.zuit.wdc044.services;

import com.zuit.wdc044.models.User;
import com.zuit.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
//@Service - to indicate that we are going to implement a service we need to use Service annotation from spring framework.
@Service
public class UserServiceImpl implements UserService{

    // @Autowired - dependency injection which means an instance of a class that implements. When it comes to the UserRepository property, it will contain an instance of the UserRepository Class.
    @Autowired
    private UserRepository userRepository;

    // We are going to define the business logic behind the createUser method from the UserService Interface.
    public void createUser(User user){
        //save() method is used to save the user in our table
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username){
        //Null can be returned as a value if the record cannot be found.
        return Optional.ofNullable(userRepository.findByUsername(username));
    }
}
