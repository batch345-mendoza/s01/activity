package com.zuit.wdc044.repositories;

import com.zuit.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Object>{
    //in this we can use Query Methods like in mongoose.https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
    // You refract or edit query methods. by following the naming conventions.
    User findByUsername(String username);
}
