package com.zuit.wdc044.repositories;

import com.zuit.wdc044.models.Post;
// for us to use CRUD.
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.zuit.wdc044.models.User;

//@Repository - interface that has marked of @Repository contains methods for database manipulation.
// by extending or inheriting CrudRepository, PostRepository will inherit its pre-defined methods for creating, retrieving, updating and deleting records. We can actually perform CRUD operations.
@Repository
public interface PostRepository extends CrudRepository<Post, Object>{
    Iterable<Post> findByUser(User user);
}
