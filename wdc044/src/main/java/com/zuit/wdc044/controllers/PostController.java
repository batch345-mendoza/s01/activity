package com.zuit.wdc044.controllers;

import com.zuit.wdc044.models.Post;
import com.zuit.wdc044.services.PostServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostServiceImpl postServiceImpl;
    //Route for creating a new post

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity <?> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        postServiceImpl.createPost(stringToken, post);

        return new ResponseEntity<>("Post Created Successfully!", HttpStatus.CREATED);
    }

    //Route for getting all posts
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<?> getPosts(){
        return new ResponseEntity<>(postServiceImpl.getPosts(), HttpStatus.OK);
    }

    //Route for editing a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<?> updatePost(@PathVariable Long postid, @RequestHeader (value = "Authorization") String stringToken, @RequestBody Post post){
        return postServiceImpl.updatePost(postid, stringToken, post);
    }

    //Route for Deleting a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePost(@PathVariable Long postid, @RequestHeader (value = "Authorization") String stringToken){
        return postServiceImpl.deletePost(postid, stringToken);
    }

    //Route for getting user's post
    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    public ResponseEntity<?> getMyPosts(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(postServiceImpl.getMyPosts(stringToken),HttpStatus.OK);
    }

}
